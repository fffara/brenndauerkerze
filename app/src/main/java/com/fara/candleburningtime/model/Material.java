package com.fara.candleburningtime.model;

//ich bin eine Klasse, die man vergleichen kann siehe implements Comparable
public class Material implements Comparable <Material> {
    private String name;
    private double density;
    private double burningRate;

    public Material(String name, double density, double burningRate) {
        this.name = name;
        this.density = density;
        this.burningRate = burningRate;
    }

    public double getDensity(){
        return density;
    }


    @Override
    public String toString() {
        return name ;
    }

    public double getBurningRate() {
        return burningRate;
    }

    //Verlgeiche nach Namen=> ich kann in der CandleActivity jetzt Collections.sort() nutzen für meine ArrayList
    //rufe die compareTo Methode auf und vergleiche mich selber mit dem außenstehenden namen     =>     -1, 0, 1
    @Override
    public int compareTo(Material other) {
       return this.name.compareTo(other.name);
    }
}

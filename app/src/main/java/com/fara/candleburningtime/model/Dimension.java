package com.fara.candleburningtime.model;

public class Dimension {
    private double length;
    private double diameter;

    public Dimension(double length, double diameter) {
        this.length = length;
        this.diameter = diameter;
    }

    public double getLength() {
        return length;
    }

    public double getDiameter() {
        return diameter;
    }


}

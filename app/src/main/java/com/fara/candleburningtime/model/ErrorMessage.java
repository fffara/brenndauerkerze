package com.fara.candleburningtime.model;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class ErrorMessage {

    //Davon soll keiner ein neues Objekt erzuegen => privater Konstruktor
    //Es können nur die statischen Methoden genutzt werden
    private ErrorMessage(){

    }

    public static void show(Context context, int resId){
        Toast toast = Toast.makeText(context, resId, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}

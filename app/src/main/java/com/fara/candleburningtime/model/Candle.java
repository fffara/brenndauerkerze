package com.fara.candleburningtime.model;

import android.util.Log;

public class Candle {
    private Dimension dimension;
    private Material material;

    public Candle(Dimension dimension, Material material) {
        this.dimension = dimension;
        if(material != null){
            this.material = material;
        }
        else{ //Komposition
            throw new NullPointerException();
        }
    }

    public Dimension getDimension(Dimension dimension) {
        return dimension;
    }

    public double getWeight() {
        double weight;
        double length = dimension.getLength();
        double diameter = dimension.getDiameter();
        double density = material.getDensity();
        weight = (Math.PI * length * diameter * diameter) / 4 * density;
        Log.i("Gewicht in g", "" + weight);
        return weight;

    }

    public double getBurningTime() {
        double time = 0.0; //in std
        double weight = getWeight();
        if (weight >= 0) {
            time = weight / material.getBurningRate();
        }
        return time;
    }

    public String getBurningTimeString(String formatString) {
        int minute = (int) Math.round(getBurningTime() * 60);
        int hour = minute/60;
        minute = minute % 60;
        return String.format(formatString,hour,minute);
    }
}


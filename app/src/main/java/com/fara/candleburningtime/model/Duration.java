package com.fara.candleburningtime.model;

public class Duration {
    private int hours;
    private int minutes;

    //nur intern aufrufbar
    public Duration(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

    //macht die Umleitung und ruft den den Constructor auf
    public static Duration of(int hours, int minutes){
        return new Duration(hours, minutes);
    }

    public static Duration ofDecimal(double decimalTime){
        int hours = (int) decimalTime;
        int minutes = (int) Math.round((decimalTime - hours) * 60);
        return new Duration(hours,minutes);
    }
    public String asFormattedString(){
        return String.format("%02d:%02d",hours, minutes);
    }
}

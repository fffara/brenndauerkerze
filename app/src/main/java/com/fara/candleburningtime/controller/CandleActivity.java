package com.fara.candleburningtime.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.fara.candleburningtime.R;
import com.fara.candleburningtime.exceptions.IncorrectInputException;
import com.fara.candleburningtime.model.Candle;
import com.fara.candleburningtime.model.Dimension;
import com.fara.candleburningtime.model.ErrorMessage;
import com.fara.candleburningtime.model.Material;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CandleActivity extends AppCompatActivity {

    private Spinner spinnerMaterial;
    private EditText editTextLength;
    private EditText editTextDiameter;
    private TextView textViewBurningTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //holt sich die Oberflächenelemente und packt sie in Instanzvariablen
        initializeView();

        List<Material> materialList = new LinkedList<>();
        materialList.add(new Material(getString(R.string.paraffin_wax), 0.9, 7.5));
        materialList.add(new Material(getString(R.string.stearin), 0.93, 6.5));
        materialList.add(new Material(getString(R.string.beeswax), 0.95, 4.0));

        Spinner spinnerListMaterial = findViewById(R.id.spinnerListMaterial);
        spinnerListMaterial.setAdapter(new ArrayAdapter<Material>(this, android.R.layout.simple_list_item_1, materialList));
    }

    //boote die Seite
    public void initializeView(){
        editTextLength = findViewById(R.id.editTextLength);
        editTextDiameter = findViewById(R.id.editTextDiameter);
        textViewBurningTime = findViewById(R.id.textViewBurningTimeResult);
        spinnerMaterial = findViewById(R.id.spinnerListMaterial);
        fillMaterialSpinner();
        clearView();
    }

    private void fillMaterialSpinner(){
        List <Material> materials = new ArrayList<>();
        materials.add(new Material(getString(R.string.paraffin_wax), 0.9, 7.5));
        materials.add(new Material(getString(R.string.stearin), 0.93, 6.5));
        materials.add(new Material(getString(R.string.beeswax), 0.95, 4.0));
        Collections.sort(materials);

        Spinner spinnerListMaterial = findViewById(R.id.spinnerListMaterial);
        spinnerListMaterial.setAdapter(new ArrayAdapter<Material>(this, android.R.layout.simple_list_item_1, materials));
    }

    public void onClickReset(View view) {
       clearView();
    }

    public void clearView(){
        updateCandleBurningTimeView(String.format(getString(R.string.burning_time_format),0,0));
        setEditText(R.id.editTextDiameter,"0");
        setEditText(R.id.editTextLength,"0");
        Spinner spinnerListMaterial = findViewById(R.id.spinnerListMaterial);
        spinnerListMaterial.setSelection(0);
    }

    //Beim Klicken des Buttons Berechne/Calculate aufgerufen
    public void onClickCalculate(View view) {
        Candle candle = getCandle();
        updateView(candle);
    }

    //Candleobjekt erzeugt, über getEditText() Parameter geholt (über jeweilige id)
    public Candle getCandle() {
        Dimension dimension = getDimension();
        Material material = getSelectedMaterial();
        return new Candle(dimension, material);
    }

    private void updateView(Candle candle) {
        if (candle != null) {
            updateCandleBurningTimeView(candle.getBurningTimeString(getString(R.string.burning_time_format)));
        }
    }


    private void updateCandleBurningTimeView(String burningTimeString) {
        TextView textview = findViewById(R.id.textViewBurningTimeResult);
        textview.setText(burningTimeString);
    }



    private Dimension getDimension() {
        Dimension dimension = null;
        try {
            double length = getLength();
            double diameter = getDiameter();
            return new Dimension(length, diameter);
        } catch (IncorrectInputException e) {
            showAndLogErrorMessage(e.getMessage());
        }
        return dimension;
    }

    // TODO: 19.05.2021 verlgeiche mit Frage zur Nutzung 
    private double getDiameter() {
        try{
            return getEditText(R.id.editTextDiameter);
        } catch (NumberFormatException e){
            ErrorMessage.show(this,R.string.error_msg_diameter);
            return 0.0;
        }

    }

    // TODO: 19.05.2021 Frage zur Nutztung 
    private double getLength() {
        String inputString = editTextLength.getText().toString();
        try{
            return Double.parseDouble(inputString);
        } catch (NumberFormatException e){
            ErrorMessage.show(this, R.string.error_msg_length); //Fehlermeldung in der strings.xml , über statische Methode in der Klasse ErrorMessage aufrufbar
            return 0.0;
        }
    }

    //holt Eingabe anhand der id und verarbeitet diese und gibt sie weiter an das Candleobjekt, nutze ich immer wieder
    public double getEditText(int id) {
        EditText editText = findViewById(id);
        String s = editText.getText().toString();
        double d = Double.parseDouble(s);
        return d;
    }

    //setzt die eingabe auf den String text, bekommt text aus der OnClickReset => setEditText(R.id.editTextDiameter,"0")
    public void setEditText(int id, String text) {
        EditText editText = findViewById(id);
        editText.setText(text);
    }

    public Material getSelectedMaterial() {
        Spinner spinnerListMaterial = findViewById(R.id.spinnerListMaterial);
        Material material = (Material) spinnerListMaterial.getSelectedItem();
        return material;
    }

    //Methode für Fehlermeldung
    public void showAndLogErrorMessage(String message) {
        Log.e("ERROR", message);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}